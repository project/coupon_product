
/**
 * Put a hint into the input field
 */
Drupal.behaviors.coupon_product_hintable = function(context) {
  $('#coupon-product-cart-pane-form:not(.coupon-product-hintable-processed)', context).addClass('coupon-product-hintable-processed').each(function() {
    // Initialize
    $(this).find('input.auto-hint').each(function(){
      if($(this).attr('title') == ''){ return; }
      $(this).val($(this).attr('title'));
      $(this).addClass('auto-hint');
    });
  
    // Handle focus
    $(this).find('input.auto-hint').focus(function(){
      if($(this).val() == $(this).attr('title')){
        $(this).val('');
        $(this).removeClass('auto-hint');
      }
    });

    // Handle blur
    $(this).find('input.auto-hint').blur(function(){
      if($(this).val() == '' && $(this).attr('title') != ''){
        $(this).val($(this).attr('title'));
        $(this).addClass('auto-hint');
      }
    });
  });
}

