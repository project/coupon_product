
Coupon as a product

Relies on stock:
* stock disabled - the coupon is valid forever
* stock enabled - the coupon stock is the number of times it's allowed

Coupon will not be applicable if:
* It's detected but the node is not published
* It's published but out of stock

Known Issues:
* When two users are trying to use the same coupon they get into a racing
condition.

They both can add it in the shopping cart, but only one of them should be able
to use it (submit the order).

The same problem is there for products in general, it's not just this module
issue.

Make sure:
- Modify your email templates to show coupon info

TODO:
// TODO: Fixed amount coupons should check a minimum order total before applied
// TODO: convert order coupons into line items on checkout complete
// TODO: convert coupon product order items into line items when saving an
order on the administration screen
// TODO: bulk generate coupons
// TODO: purchase the coupon as a gift somehow? Generate unique URL?
// TODO: if there are orders with this coupon don't allow to edit it anymore


